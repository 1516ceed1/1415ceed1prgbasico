/*
 * Programa en java que calcula y muestra la suma de los elementos del vector
 * de enteros
 */
package org.ceedcv.ceed1prgbasico.tema5;

public class VectorSuma {

    public static void main(String[] args) {

        int suma = 0;
        int vector[] = {2, 3, 4};

        for (int i = 0; i < vector.length; i++) {
            suma = suma + vector[i];
        }
        System.out.println("Suma: " + suma);
    }

}
