/*
 * Programa en java que calcula y muestra la suma de los elementos del hashset
 * de enteros
 */
package org.ceedcv.ceed1prgbasico.tema5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class HashSuma {

    public static void main(String[] args) {

        int suma = 0;
        int numero = 0;
        ArrayList hs = new ArrayList();

        hs.add(2);
        hs.add(4);
        hs.add(6);

        Iterator it = hs.iterator();

        while (it.hasNext()) {
            numero = (int) it.next();
            suma = suma + numero;
        }

        System.out.println("Suma: " + suma);
    }

}
