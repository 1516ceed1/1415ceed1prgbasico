/*
 * Hacer una funcion que muestre el testo hola.
 */
package org.ceedcv.ceed1prgbasico.tema4;

public class FuncionHola {

    static void hola() {
        System.out.println("Hola");
    }

    public static void main(String[] args) {
        hola();
    }

}
