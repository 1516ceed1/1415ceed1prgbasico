/*
 * Hacer una funcion que sume dos numeros.
 */
package org.ceedcv.ceed1prgbasico.tema4;

public class FuncionSuma {

    static int suma(int i, int j) {
        return i + j;
    }

    public static void main(String[] args) {
        System.out.println("Suma: " + suma(3, 2));
    }

}
