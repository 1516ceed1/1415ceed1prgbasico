package org.ceedcv.ceed1prgbasico.tema3;

/*
 * Hacer un programa que sume los numeros del 1 al 3
 */
public class WhileSuma10 {

    public static void main(String argv[]) {

        int suma = 0;

        int contador = 1;

        while (contador <= 3) { // Si es falso se sale
            suma = suma + contador;
            contador++;
        }

        System.out.println("Suma: " + suma);

    }
}
