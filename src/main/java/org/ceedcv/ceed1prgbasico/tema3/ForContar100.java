/*
 * Hacer un programa que muestre los numeros del 1 al 100.
 * Con while
 * Ejemplo: 1,2, ... 100
 */
package org.ceedcv.ceed1prgbasico.tema3;

public class ForContar100 {

    public static void main(String[] args) {
        int i;
        for (i = 1; i <= 100; i++) {
            System.out.println(i);
        }

    }

}
