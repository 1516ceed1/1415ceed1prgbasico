/*
 * Hacer un programa que muestre los numeros del 1 al 100.
 * Con while
 * Ejemplo: 1,2, ... 100
 */
package org.ceedcv.ceed1prgbasico.tema3;

public class WhileContar100 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int i = 1;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

    }

}
