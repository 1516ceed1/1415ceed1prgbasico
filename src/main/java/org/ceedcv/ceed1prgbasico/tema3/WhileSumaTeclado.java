package org.ceedcv.ceed1prgbasico.tema3;

import java.util.Scanner;

public class WhileSumaTeclado {

    public static void main(String[] args) {

        int suma = 0;
        int numero;

        System.out.print("Numero: ");
        Scanner sc = new Scanner(System.in);

        numero = sc.nextInt();
        while (numero != 0) {

            suma = suma + numero;

            System.out.print("Numero: ");
            numero = sc.nextInt();
        }
        System.out.println("La suma es: " + suma);

    }

}
