/* Clase Persona.
 */
package org.ceedcv.ceed1prgbasico.tema6;

public class Persona {

    private String id;
    private String nombre;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
