/*
 * Hacer un programa que grabe un alumno y despues lo lea de un fichero.
 */
package org.ceedcv.ceed1prgbasico.tema6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FicheroCsvObjeto {

    public static void main(String[] args) throws IOException {

        Persona persona = new Persona();
        persona.setId("1");
        persona.setNombre("Paco");
        grabar(persona);

        Persona persona1 = new Persona();
        persona1 = leer();
        System.out.println("Persona: " + persona1.getId() + " " + persona1.getNombre());

    }

    public static void grabar(Persona persona) throws IOException {
        FileWriter fw = null;

        fw = new FileWriter("personas.csv");
        fw.write(persona.getId() + ";" + persona.getNombre() + ";\n");
        fw.close();

    }

    public static Persona leer() throws IOException {

        FileReader fr = new FileReader("personas.csv");
        BufferedReader br = new BufferedReader(fr);
        String linea = br.readLine();
        StringTokenizer st = new StringTokenizer(linea, ";");

        String id = st.nextToken();
        String nombre = st.nextToken();

        Persona persona = new Persona();
        persona.setId(id);
        persona.setNombre(nombre);

        fr.close();

        return persona;

    }

}
